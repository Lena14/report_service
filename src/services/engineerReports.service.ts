"use strict";

import { Service } from "moleculer";
import engineerReportsManager from "../managers/engineerReportsManager";
import { CONSTANTS } from "crm-utilities";
const { ENG_REPORT: { ACTION_TYPE, CHART_GROUP_BY } } = CONSTANTS;

class EngineerReportsService extends Service {
  constructor(broker) {
    super(broker);
    this.engineerReportsManager = engineerReportsManager;
    this.parseServiceSchema({
      name: "engineerReportsService",
      version: "v1",
      actions: {
        getEngineersChart: {
          requiredCompanyAuth: true,
          params: {
            start: { type: "date", optional: false, convert: true },
            end: { type: "date", optional: false, convert: true },
            regionId: { type: "string", optional: true, empty: false },
            cityId: { type: "string", optional: true, empty: false },
            tariffId: { type: "string", optional: true, empty: false },
            groupBy: { type: "enum", optional: false, empty: false, values: [CHART_GROUP_BY.TARIFF, CHART_GROUP_BY.FULLNAME] },
          },
          handler: this.engineerReportsManager.getEngineersChart,
        },

        getSingleEngChart: {
          requiredCompanyAuth: true,
          params: {
            start: { type: "date", optional: false, convert: true },
            end: { type: "date", optional: false, convert: true },
            regionId: { type: "string", optional: true, empty: false },
            cityId: { type: "string", optional: true, empty: false },
            tariffId: { type: "string", optional: true, empty: false },
            groupBy: { type: "enum", optional: false, empty: false, values: [CHART_GROUP_BY.TARIFF, CHART_GROUP_BY.ACTION_TYPE] },
            engineerId: { type: "string", optional: false, empty: false },
          },
          handler: this.engineerReportsManager.getSingleEngChart,
        },

        createEngPushReqReport: {
          requiredCompanyAuth: true,
          params: {
            engineerId: { type: "number", optional: false, integer: true, positive: true },
            fullName: { type: "string", optional: false, empty: false },
            requestId: { type: "number", optional: false, integer: true, positive: true },
            regionId: { type: "number", optional: false, integer: true, positive: true },
            cityId: { type: "number", optional: false, integer: true, positive: true },
            tariffId: { type: "number", optional: false, integer: true, positive: true },
            tariffName: { type: "string", optional: false, empty: false },
            actionType: { type: "enum", optional: false, empty: false, values: [ACTION_TYPE.PUSHED] },
          },
          handler: this.engineerReportsManager.createEngPushReqReport,
        },

        createEngAssignReqReport: {
          requiredCompanyAuth: true,
          params: {
            assignedRequests: {
              type: "array",
              optional: false,
              min: 1,
              items: { type: "object", optional: false, props: {
                engineerId: { type: "number", optional: false, integer: true, positive: true },
                fullName: { type: "string", optional: false, empty: false },
                requestId: { type: "number", optional: false, integer: true, positive: true },
                regionId: { type: "number", optional: false, integer: true, positive: true },
                cityId: { type: "number", optional: false, integer: true, positive: true },
                tariffId: { type: "number", optional: false, integer: true, positive: true },
                tariffName: { type: "string", optional: false, empty: false },
                actionType: { type: "enum", optional: false, empty: false, values: [ACTION_TYPE.ASSIGNED] },
              } },
            },
          },
          handler: this.engineerReportsManager.createEngAssignReqReport,
        },
      },
    });
  }
}
export = EngineerReportsService;
