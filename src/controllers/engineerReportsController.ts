import { iEngineerReports } from "../types";
import db from "../db/models";
import { Exception } from "../helpers/exceptions";
const { Sequelize: { Op, literal } } = db;
const engineerReportsModel = db.EngineerReports;


class EngineerReportsController {
  public constructor() {
    this.getEngineersChart = this.getEngineersChart.bind(this);
    this.createEngineerReports = this.createEngineerReports.bind(this);
  }

  public async getEngineersChart(params: iEngineerReports.GetChartByEngineerParams): Promise<iEngineerReports.RetChartProps[]> {
    try {
      const { actionType, cityId, end, engineerId, groupBy, regionId, start, tariffId } = params;

      const where: iEngineerReports.GetOpChartsWhere = {
        createdAt: { [Op.and]: [{ [Op.gte]: start }, { [Op.lte]: end }] },
      };

      if (actionType) {
        where.actionType = actionType;
      }
      if (tariffId) {
        where.tariffId = tariffId;
      }
      if (regionId) {
        where.regionId = regionId;
        if (cityId) {
          where.cityId = cityId;
        }
      }
      if (engineerId) {
        where.engineerId = engineerId;
      }

      return await engineerReportsModel.findAll({
        where,
        group: [groupBy],
        order: [["count", "DESC"]],
        attributes: [[groupBy, "name"], [literal("COUNT(id)"), "count"]],
      });

    } catch (err) {
      throw new Exception(err);
    }
  }

  public async createEngineerReports(requests: iEngineerReports.CreateEngReqReportParams[]): Promise<void> {
    try {

      for (const request of requests) {
        await engineerReportsModel.create(request);
      }

    } catch (err) {
      throw new Exception(err);
    }
  }

}

export default new EngineerReportsController();
