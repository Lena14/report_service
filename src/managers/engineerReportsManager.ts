import EngineerReportsController from "../controllers/engineerReportsController";
import { iEngineerReports } from "../types";
import { CONSTANTS } from "crm-utilities";
const { ENG_REPORT: { ACTION_TYPE, CHART_GROUP_BY } } = CONSTANTS;


class EngineerReportsManager {
  public constructor() {
    this.getEngineersChart = this.getEngineersChart.bind(this);
    this.getSingleEngChart = this.getSingleEngChart.bind(this);
    this.createEngPushReqReport = this.createEngPushReqReport.bind(this);
    this.createEngAssignReqReport = this.createEngAssignReqReport.bind(this);
  }

  public async getEngineersChart(ctx: iEngineerReports.GetChartByEngineerCtx): Promise<iEngineerReports.RetChartProps[]> {
    const { meta: { data: { filterByRegion, regionId } }, params } = ctx;

    if (filterByRegion) {
      params.regionId = regionId.toString();
    }
    params.actionType = ACTION_TYPE.PUSHED;
    return await EngineerReportsController.getEngineersChart(params);
  }

  public async getSingleEngChart(ctx: iEngineerReports.GetChartByEngineerCtx): Promise<iEngineerReports.RetChartProps[]> {
    const { meta: { data: { filterByRegion, regionId } }, params } = ctx;

    if (filterByRegion) {
      params.regionId = regionId.toString();
    }
    if (params.groupBy === CHART_GROUP_BY.TARIFF) {
      params.actionType = ACTION_TYPE.PUSHED;
    }
    return await EngineerReportsController.getEngineersChart(params);
  }

  public async createEngPushReqReport(ctx: iEngineerReports.CreateEngPushReqReportCtx): Promise<{ message: string }> {
    await EngineerReportsController.createEngineerReports([ctx.params]);
    return { message: "Engineer report successfully created" };
  }

  public async createEngAssignReqReport(ctx: iEngineerReports.CreateEngAssignReqReportCtx): Promise<{ message: string }> {
    await EngineerReportsController.createEngineerReports(ctx.params.assignedRequests);
    return { message: "Engineer report successfully created" };
  }
}
export default new EngineerReportsManager();
