export const ERRORS = {
  ERR_MSG: {
    TRY_AGAIN: "Something goes wrong, try again",
    NOT_FOUND_REPORT: "Report not found.",
  },
  ERR_CODE: {
    CONFLICT: 409,
    NOT_FOUND: 404,
  },
};

