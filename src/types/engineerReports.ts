import { GenericObject } from "moleculer";

export namespace iEngineerReports{

    export interface MetaData {
        userId: number;
        companyId: number;
        companyRole: string;
        departmentId: number;
        departmentRole: string;
        permissions: object[];
        permissionsGroups: object[];
        allGroupsPermissions: object[];
        departments: number[];
        regionId: number;
        filterByRegion: boolean;
    }

    export interface BaseCtx {
        meta: { data: MetaData };
        call<T, P extends GenericObject = GenericObject>(actionName: string, params?: P, opts?: GenericObject): PromiseLike<T>;
    }

    export interface BaseGetChart {
        start: Date;
        end: Date;
    }

    export interface GetChartByEngineerCtx extends BaseCtx {
        params: GetChartByEngineerParams;
    }
    export interface GetChartByEngineerParams extends BaseGetChart{
        regionId?: string;
        cityId?: string;
        tariffId?: string;
        engineerId?: string;
        groupBy: string;
        actionType: string;
    }

    export interface CreateEngPushReqReportCtx extends BaseCtx {
        params: CreateEngReqReportParams;
    }
    export interface CreateEngAssignReqReportCtx extends BaseCtx {
        params: { assignedRequests: CreateEngReqReportParams[] };
    }
    export interface CreateEngReqReportParams{
        requestId: number;
        regionId: number;
        cityId: number;
        tariffId: number;
        tariffName: string;
        engineerId: number;
        fullName: string;
        actionType: string;
    }

    export interface RetChartProps {
        name: string;
        count: string;
    }

    export interface GetOpChartsWhere {
        createdAt: object;
        cityId?: string;
        regionId?: string;
        tariffId?: string;
        actionType?: string;
        engineerId?: string;
    }
}

