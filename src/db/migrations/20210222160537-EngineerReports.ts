import { CONSTANTS } from "crm-utilities";
const {
  ENG_REPORT: { ACTION_TYPE },
} = CONSTANTS;

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("EngineerReports", {
      id: {
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      engineerId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      fullName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      requestId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      regionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      cityId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      tariffId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      tariffName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      actionType: {
        type: Sequelize.ENUM,
        values: [ACTION_TYPE.PUSHED, ACTION_TYPE.ASSIGNED],
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      },
    }, { schema: "public" });
  },

  async down (queryInterface) {
    await queryInterface.dropTable("EngineerReports");
  },
};
