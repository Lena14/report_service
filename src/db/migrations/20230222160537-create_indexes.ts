
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(`CREATE INDEX date_action ON "EngineerReports" ("createdAt", "actionType")`);
    await queryInterface.sequelize.query(`CREATE INDEX date_action_engineer ON "EngineerReports" ("createdAt", "actionType", "engineerId")`);
    await queryInterface.sequelize.query(`CREATE INDEX engineer_chart ON "EngineerReports" ("createdAt", "actionType", "tariffId", "regionId", "cityId", "engineerId")`);
  },

  async down (queryInterface) {
    await queryInterface.removeIndex("EngineerReports", "engineer_chart");
    await queryInterface.removeIndex("EngineerReports", "date_action");
    await queryInterface.removeIndex("EngineerReports", "date_action_engineer");
  },
};
